/*
 * piradio.h
 *
 *  Created on: Sep 22, 2021
 *      Author: george
 */

#ifndef PIRADIO_H_
#define PIRADIO_H_

#include <linux/spinlock.h>
#include <linux/dmaengine.h>
#include <linux/dma-mapping.h>
#include <linux/of_dma.h>
#include <linux/types.h>
#include <linux/sched/task.h>
#include <linux/sockios.h>
#include <linux/kfifo.h>

#define XIL_CHANN_EMULATOR0          0xA0060000
#define XIL_FIR_FILTER0              0xA0050000
#define XIL_MODULATOR0               0xA0030000
#define XIL_CSMA_DELAY0              0xA00A0000
#define XIL_CSMA_STATUS0             0xA00B0000
#define XIL_SYNC_THRESH0             0xB00B0000

#define XIL_CHANN_EMULATOR1          0xB0000000
#define XIL_FIR_FILTER1              0xB0010000
#define XIL_MODULATOR1               0xA00E0000
#define XIL_CSMA_DELAY1              0xB0050000
#define XIL_CSMA_STATUS1             0xB0060000
#define XIL_SYNC_THRESH1             0xB0080000

#define XIL_RESET                    0xFF0A0054


#define PIRADIO_SENT_QUEUE_LENGTH            128
#define PIRADIO_PENDING_QUEUE_LENGTH         128
#define PIRADIO_WAIT_QUEUE_LENGTH            2
#define PIRADIO_MAX_CSMA_TRIES               10   /*Maximum number of attempts to restart CSMA timer if channel always occupied*/
#define PIRADIO_MAX_TX_TRIES                 10   /*Maximum number of attempts to transmit if no ACK was received*/
#define PIRADIO_CSMA_THRESHOLD               1000000
#define PIRADIO_CSMA_BACKOFF_MAX             100000 /* Maximum nanoseconds to wait for CSMA engine recheck*/
#define PIRADIO_ACK_HOLDOFF                  700000 /* Maximum nanoseconds to wait for ACK arrival */

#define PIRADIO_HW_ALEN                      6
#define PIRADIO_HDR_LEN                      sizeof(struct piradio_hdr)

#define PIRADIO_ACK_TYPE                     0x2000
#define PIRADIO_SYNC_THRESHOLD               0x61a8 //25000
#define PIRADIO_MAX_MTU

typedef enum{
	PIR_T_ACK   = 0,
	PIR_T_DATA  = 1
}piradio_hdr_t;

struct piradio_hdr {
	__u32           p_seq_num;              /* sequence number      */
	piradio_hdr_t   p_type;                 /* piradio packet type  */
	s64             p_time;
} __attribute__((packed));

struct config_data{
	__u8 *data;
	__u16 length;
};

typedef enum{
	PIRADIO_RESET_MOD         = SIOCDEVPRIVATE + 1,
	PIRADIO_CONFIG_FRAMER     = SIOCDEVPRIVATE + 2,
	PIRADIO_CONFIG_CORRELATOR = SIOCDEVPRIVATE + 3,
	PIRADIO_CONFIG_FIR        = SIOCDEVPRIVATE + 4,
	PIRADIO_CONFIG_FFT_TX     = SIOCDEVPRIVATE + 5,
	PIRADIO_CONFIG_FFT_RX     = SIOCDEVPRIVATE + 6,
	PIRADIO_CONFIG_CAPT_SYST  = SIOCDEVPRIVATE + 7,
	PIRADIO_CONFIG_FREQ_OFF   = SIOCDEVPRIVATE + 8,
	PIRADIO_CONFIG_ACK        = SIOCDEVPRIVATE + 9,
	PIRADIO_CONFIG_CSMA       = SIOCDEVPRIVATE + 10,
	PIRADIO_CONFIG_STHRESH    = SIOCDEVPRIVATE + 11,
}cmd_t;

struct stats{
	__u64	rx_packets;
	__u64	tx_packets;
	__u64	rx_bytes;
	__u64	tx_bytes;
	__u64	rx_errors;
	__u64	tx_errors;
	__u64	rx_dropped;
	__u64	tx_dropped;
	__u64	retransmissions;
};

struct piradio_dma_chann {

	struct dma_chan *channel_p;
	struct completion cmp;
	dma_cookie_t cookie;
	dma_addr_t mapping;
	__u8 *rx_buffer;
	//__u8 **tx_ring;
	//__u8 **rx_ring;
};

struct piradio_priv {
	spinlock_t lock;
	spinlock_t timer_lock;
	struct net_device *netdev;
	struct piradio_dma_chann tx_dma;
	struct piradio_dma_chann rx_dma;
	struct piradio_dma_chann framer_config_dma;
	struct piradio_dma_chann fft_tx_dma;
	struct piradio_dma_chann correlator_dma;
	struct piradio_dma_chann correlator2_dma;
	struct piradio_dma_chann fft_rx_dma;
	struct piradio_dma_chann fft_rx2_dma;
	struct platform_device *pdev;
	struct stats link_stats;
	struct sk_buff *last_skb;
//	struct task_struct *tx_task;
	struct dma_async_tx_descriptor *chan_desc;
	__u32 sequence_num;

	__u8 *ack_buffer;
	struct piradio_hdr *ack_hdr;

	__u16 csma_tries;
	__u16 tx_tries;
	struct hrtimer csma_timer;
	struct hrtimer ack_timer;
	__u8 ack_enabled;

	void __iomem *chann_emm_base;
	//void __iomem *fir_filt_base;
	void __iomem *modul_base;
	void __iomem *csma_delay_base;
	void __iomem *csma_status_base;
	void __iomem *sync_thresh_base;
};

//struct skb_priv{
//	struct list_head node;
//	struct sk_buff *nskb;
//};

static void __iomem *reset_base;

static void rx_cmplt_callback(void *completion);

static enum hrtimer_restart csma_timer_handler(struct hrtimer *timer);

static void rx_cmplt_callback0(void *completion);
static void rx_cmplt_callback1(void *completion);
int
piradio_prep_tx(struct sk_buff *skb, struct piradio_priv* piradio_p);

#endif /* PIRADIO_H_ */
