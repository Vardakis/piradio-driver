#include <linux/kernel.h>
#include <linux/jiffies.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/string.h>
#include <linux/socket.h>
#include <linux/errno.h>
#include <linux/fcntl.h>
#include <linux/in.h>
#include <linux/of_dma.h>

#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/inet.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/skbuff.h>
#include <linux/ethtool.h>
#include <net/sock.h>
#include <net/checksum.h>
#include <linux/if_ether.h> /* For the statistics structure. */
#include <linux/if_arp.h> /* For ARPHRD_ETHER */s
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/percpu.h>
#include <linux/net_tstamp.h>
#include <net/net_namespace.h>
#include <linux/u64_stats_sync.h>
#include <linux/delay.h>
#include <linux/types.h>
#include <linux/random.h>
#include <net/inet_ecn.h>
#include "piradio1.h"
#include <linux/kthread.h>
#include <linux/wait.h>
#include <linux/kfifo.h>
#include <linux/of_mdio.h>
#include <linux/of_net.h>
#include <linux/of_platform.h>
#include <linux/of_irq.h>
#include <linux/of_address.h>

#define OFDM_SYMBOL_SIZE 80
#define OFDM_FRAME_SIZE (9 * 1000)
#define OFDM_FRAME_DATA_CARRIERS (9 * 640)
MODULE_LICENSE("Dual BSD/GPL");

static inline void piradio_dma_bdout(struct piradio_dma *q,
				     off_t reg, dma_addr_t value)
{
#if defined(CONFIG_PHYS_ADDR_T_64BIT)
	writeq(value, (q->dma_regs + reg));
#else
	writel(value, (q->dma_regs + reg));
#endif
}

static void write_reg(__u32 value, void __iomem *base)
{
	writel(value, base);
}

static __u32 read_reg(void __iomem *base)
{
	return readl(base);
}

inline u32 piradio_dma_read32(struct piradio_dma *dma_obj, off_t reg){
	return ioread32(dma_obj->dma_regs + reg);
}

inline void piradio_dma_write32(struct piradio_dma *dma_obj,
				     off_t reg, u32 value)
{
	iowrite32(value, dma_obj->dma_regs + reg);
}

int piradio_dma_tx_probe(struct platform_device *pdev,
		struct piradio_dma* dma_obj, int dev_tree_idx){
	struct device_node* np = NULL;
	struct resource dmares;
	int ret;

	np = of_parse_phandle(pdev->dev.of_node, "mydms",
			dev_tree_idx);
	ret = of_address_to_resource(np, dev_tree_idx, &dmares);
	if (ret >= 0) {
		dma_obj->dma_regs = devm_ioremap_resource(&pdev->dev,
							&dmares);
		if(!dma_obj->dma_regs)
			return -1;
	}
	else{
		printk(KERN_ALERT "DMA probe problem");
		return -1;
	}
	dma_obj->irq = irq_of_parse_and_map(np, 0);
	of_node_put(np);
	spin_lock_init(&dma_obj->lock);
	printk(KERN_ALERT "DMA TX probe successful");
	return 0;
}

int piradio_dma_rx_probe(struct platform_device *pdev,
		struct piradio_dma* dma_obj, int dev_tree_idx){
	struct device_node* np = NULL;
	struct resource dmares;
	int ret;

	np = of_parse_phandle(pdev->dev.of_node, "mydms",
						dev_tree_idx);
	ret = of_address_to_resource(np, 0, &dmares);
	if (ret >= 0) {
		dma_obj->dma_regs = devm_ioremap_resource(&pdev->dev,
							&dmares);
		if(!dma_obj->dma_regs)
			return -1;
	}
	else{
		printk(KERN_ALERT "DMA probe problem");
		return -1;
	}
	dma_obj->irq = irq_of_parse_and_map(np, 0);
	of_node_put(np);
	spin_lock_init(&dma_obj->lock);
	printk(KERN_ALERT "DMA RX probe successful");
	return 0;
}

int piradio_mod_reg_probe(struct platform_device *pdev,
	void __iomem **reg_base){

	struct device_node* np = NULL;
	struct resource dmares;
	int ret;

	np = of_parse_phandle(pdev->dev.of_node, "mod_reg",
						0);
	ret = of_address_to_resource(np, 0, &dmares);
	if (ret >= 0) {
		*reg_base = devm_ioremap_resource(&pdev->dev,
							&dmares);
		if(!*reg_base)
			return -1;
	}
	else{
		printk(KERN_ALERT "MOD1 RX probe problem");
		return -1;
	}
	of_node_put(np);
	printk(KERN_ALERT "MOD1 RX probe successful");
	return 0;	
}

int piradio_dma_alloc_tx_bds(struct piradio_dma *dma_obj,
		struct net_device *ndev){
	struct piradio_priv *priv = netdev_priv(ndev);
	u32 cr;
	int i;
	dma_obj->bd_curr = 0;
	dma_obj->bd_tail = 0;

	dma_obj->bd_list = dma_alloc_coherent(ndev->dev.parent,
			sizeof(*dma_obj->bd_list) * priv->tx_bd_num, &dma_obj->bd_list_base, GFP_KERNEL);
	if (!dma_obj->bd_list)
		goto out;

	for (i = 0; i < priv->tx_bd_num; i++) {
		dma_obj->bd_list[i].next = dma_obj->bd_list_base
				+ sizeof(*dma_obj->bd_list) * ((i + 1) % priv->tx_bd_num);
	}

	cr = piradio_dma_read32(dma_obj, XAXIDMA_TX_CR_OFFSET);
	cr = (((cr & ~XAXIDMA_COALESCE_MASK))
			| ((priv->coalesce_count_tx) << XAXIDMA_COALESCE_SHIFT));
	cr = (((cr & ~XAXIDMA_DELAY_MASK))
			| (XAXIDMA_DFT_TX_WAITBOUND << XAXIDMA_DELAY_SHIFT));
	cr |= XAXIDMA_IRQ_ALL_MASK;
	piradio_dma_write32(dma_obj, XAXIDMA_TX_CR_OFFSET, cr);
	piradio_dma_bdout(dma_obj, XAXIDMA_TX_CDESC_OFFSET, dma_obj->bd_list_base);
	cr = piradio_dma_read32(dma_obj, XAXIDMA_TX_CR_OFFSET);
	piradio_dma_write32(dma_obj, XAXIDMA_TX_CR_OFFSET, cr | XAXIDMA_CR_RUNSTOP_MASK);
	return 0;
out:
	return -ENOMEM;

}

int piradio_dma_alloc_rx_bds(struct piradio_dma *dma_obj,
		struct net_device *ndev){
	struct sk_buff *skb;
	struct piradio_priv *priv = netdev_priv(ndev);
	u32 cr;
	int i;
	dma_obj->bd_curr = 0;
	dma_obj->bd_tail = priv->rx_bd_num - 1;

	dma_obj->bd_list = dma_alloc_coherent(ndev->dev.parent,
			sizeof(*dma_obj->bd_list) * priv->rx_bd_num, &dma_obj->bd_list_base, GFP_KERNEL);
	if (!dma_obj->bd_list)
		goto out;

	for (i = 0; i < priv->rx_bd_num; i++) {
		dma_obj->bd_list[i].next = dma_obj->bd_list_base
				+ sizeof(*dma_obj->bd_list) * ((i + 1) % priv->rx_bd_num);
		skb = netdev_alloc_skb(ndev,
							   priv->transfer_length);
		if (!skb)
			goto out;
		wmb();
		dma_obj->bd_list[i].phys = dma_map_single(priv->dev,
			    					skb->data,
									priv->transfer_length,
									DMA_FROM_DEVICE);
		dma_obj->bd_list[i].sw_id_offset = (phys_addr_t)skb;
		dma_obj->bd_list[i].cntrl = priv->transfer_length;
	}

	cr = piradio_dma_read32(dma_obj, XAXIDMA_RX_CR_OFFSET);
	cr = (((cr & ~XAXIDMA_COALESCE_MASK))
			| ((priv->coalesce_count_rx) << XAXIDMA_COALESCE_SHIFT));
	cr = (((cr & ~XAXIDMA_DELAY_MASK))
			| (XAXIDMA_DFT_TX_WAITBOUND << XAXIDMA_DELAY_SHIFT));
	cr |= XAXIDMA_IRQ_ALL_MASK;
	piradio_dma_write32(dma_obj, XAXIDMA_RX_CR_OFFSET, cr);

	piradio_dma_bdout(dma_obj, XAXIDMA_RX_CDESC_OFFSET, dma_obj->bd_list_base);
	cr = piradio_dma_read32(dma_obj, XAXIDMA_RX_CR_OFFSET);
	piradio_dma_write32(dma_obj, XAXIDMA_RX_CR_OFFSET, cr | XAXIDMA_CR_RUNSTOP_MASK);
	piradio_dma_write32(dma_obj, XAXIDMA_RX_TDESC_OFFSET,
			 dma_obj->bd_list_base + sizeof(*dma_obj->bd_list) * dma_obj->bd_tail);
	return 0;
out:
	return -ENOMEM;

}

int piradio_rx(struct net_device *dev, int budget){
	dma_addr_t tail_p = 0;
	u32 status;
	int ret;
	struct piradio_priv *priv = netdev_priv(dev);
	struct axidma_bd *cur_p, *prev_p;
	u32 csum = 0;
	struct piradio_dma* dma_obj = &priv->rx_dma;
	struct sk_buff *skb, *new_skb, *pskb;
	struct piradio_hdr *prev_pir;
	struct iphdr *ip_header;
	int pkts_processed = 0;
	rmb();
	cur_p = &dma_obj->bd_list[dma_obj->bd_curr];
	status = piradio_dma_read32(dma_obj, XAXIDMA_RX_SR_OFFSET);
	piradio_dma_write32(dma_obj, XAXIDMA_RX_SR_OFFSET, status);
	while ( (pkts_processed < budget) &&
			(cur_p->status & XAXIDMA_BD_STS_COMPLETE_MASK)) {
		tail_p = dma_obj->bd_list_base + sizeof(*dma_obj->bd_list) * dma_obj->bd_curr;
		dma_unmap_single(priv->dev, cur_p->phys,
				priv->transfer_length,
				 DMA_FROM_DEVICE);
		new_skb = netdev_alloc_skb(dev, priv->transfer_length );
		if (!new_skb) {
			printk(KERN_ALERT "No mem for skb ");
			break;
		}
		skb = (struct sk_buff *)(cur_p->sw_id_offset);
		struct piradio_hdr *pir = (struct piradio_hdr *)(skb->data);
		ip_header = (struct iphdr *)(skb->data +
						 ETH_HLEN + PIRADIO_HDR_LEN);
		if(ntohs(ip_header->tot_len) <= (priv->transfer_length - ETH_HLEN - PIRADIO_HDR_LEN)){
			skb_put(skb, ntohs(ip_header->tot_len) + ETH_HLEN + PIRADIO_HDR_LEN);
			skb_pull(skb, PIRADIO_HDR_LEN);
			csum = ~crc32_le(~0, skb->data,
					skb->len);
			if(pir->p_csum != csum){
				priv->link_stats.crc_rx_errors++;
			}
			if (ip_header->ihl == 5 && ip_header->version == 4) {
				ip_header->daddr ^=
					0x00010000; /*Flip last bit of network address to fool the OS*/
				ip_header->saddr ^=
					0x00010000; /*Flip last bit of host address to fool the OS*/
				ip_header->check = 0;
				ip_header->check =
					ip_fast_csum((u8 *)ip_header, ip_header->ihl);
				skb->protocol = eth_type_trans(skb, dev);
				skb->ip_summed = CHECKSUM_UNNECESSARY;
				ret = netif_receive_skb(skb);
				if(ret == NET_RX_DROP)
					priv->link_stats.rx_dropped++;
				else if(ret == NET_RX_SUCCESS)
					priv->link_stats.rx_packets++;
			}
			else{
				printk(KERN_ALERT "Invalid header ");
			}
		}
		else{
			netdev_dbg(dev, "RX Invalid length %u", ntohs(ip_header->tot_len));
		}
		wmb();
		cur_p->phys = dma_map_single(priv->dev, new_skb->data,
						priv->transfer_length,
					   DMA_FROM_DEVICE);
		cur_p->cntrl = priv->transfer_length;
		cur_p->status = 0;
		cur_p->sw_id_offset = (phys_addr_t)new_skb;
		if (++dma_obj->bd_curr >= priv->rx_bd_num)
			dma_obj->bd_curr = 0;

		rmb();
		cur_p = &dma_obj->bd_list[dma_obj->bd_curr];
		pkts_processed++;
	}
	if (tail_p) {
		piradio_dma_bdout(dma_obj, XAXIDMA_RX_TDESC_OFFSET, tail_p);
	}
	return pkts_processed;
}

int piradio_rx_poll(struct napi_struct *napi, int quota)
{
	u32 status,control, pkts = 0;
	struct net_device *dev = napi->dev;
	struct piradio_priv *priv = netdev_priv(dev);
	struct piradio_dma* dma_obj = &priv->rx_dma;
	//spin_lock(&dma_obj->lock);
	status = piradio_dma_read32(dma_obj, XAXIDMA_RX_SR_OFFSET);
	while ((status & (XAXIDMA_IRQ_IOC_MASK | XAXIDMA_IRQ_DELAY_MASK)) &&
		       (pkts < quota)) {
		piradio_dma_write32(dma_obj, XAXIDMA_RX_SR_OFFSET, status);
		pkts += piradio_rx(dev, quota - pkts);
		status = piradio_dma_read32(dma_obj, XAXIDMA_RX_SR_OFFSET);
	}
	if(pkts < quota){
		napi_complete(napi);
		control = piradio_dma_read32(dma_obj, XAXIDMA_RX_CR_OFFSET);
		control |= (XAXIDMA_IRQ_IOC_MASK | XAXIDMA_IRQ_DELAY_MASK);
		piradio_dma_write32(dma_obj, XAXIDMA_RX_CR_OFFSET, control);
		control = piradio_dma_read32(dma_obj, XAXIDMA_RX_SR_OFFSET);
		if((status & (XAXIDMA_IRQ_IOC_MASK | XAXIDMA_IRQ_DELAY_MASK))){
			napi_reschedule(napi);
			control = piradio_dma_read32(dma_obj, XAXIDMA_RX_CR_OFFSET);
			control &= ~(XAXIDMA_IRQ_IOC_MASK | XAXIDMA_IRQ_DELAY_MASK);
			piradio_dma_write32(dma_obj, XAXIDMA_RX_CR_OFFSET, control);
		}
	}
	//spin_unlock(&dma_obj->lock);
	return pkts;
}

irqreturn_t rx_cmplt_callback1(int irq, void *_dev){
	dma_addr_t tail_p = 0;
	u32 status,control;
	struct net_device *dev = (struct net_device *)_dev;
	struct piradio_priv *priv = netdev_priv(dev);
	struct axidma_bd *cur_p;
	struct piradio_dma* dma_obj = &priv->rx_dma;
	struct sk_buff *skb, *new_skb;
	struct piradio_hdr *pir;
	struct iphdr *ip_header;
	status = piradio_dma_read32(dma_obj, XAXIDMA_RX_SR_OFFSET);
	if (status & (XAXIDMA_IRQ_IOC_MASK | XAXIDMA_IRQ_DELAY_MASK)) {
		control = piradio_dma_read32(dma_obj, XAXIDMA_RX_CR_OFFSET);
		control &= ~(XAXIDMA_IRQ_IOC_MASK | XAXIDMA_IRQ_DELAY_MASK);
		piradio_dma_write32(dma_obj, XAXIDMA_RX_CR_OFFSET, control);
		napi_schedule(&priv->napi);
	}
	if (!(status & XAXIDMA_IRQ_ALL_MASK)){
		netdev_dbg(priv->netdev,"Error 1");
		return IRQ_NONE;
	}

	if (status & XAXIDMA_IRQ_ERROR_MASK) {
		netdev_dbg(priv->netdev,"Error 2");
		return IRQ_HANDLED;
	}
	return IRQ_HANDLED;
}

static void remap_devs(void)
{
	reset_base = ioremap(XIL_RESET, 128);
}

static void unmap_devs(void)
{
}

int piradio1_config(struct net_device *dev)
{
	return 0;
}

irqreturn_t tx_cmplt_callback(int irq, void *_dev){
	u32 status;
	struct axidma_bd *cur_p;
	struct net_device *dev = (struct net_device *)_dev;
	struct piradio_priv *priv = netdev_priv(dev);
	struct piradio_dma* dma_obj = &priv->tx_dma;
	struct piradio_hdr *pir;
	struct sk_buff *skb;
	status = piradio_dma_read32(dma_obj, XAXIDMA_TX_SR_OFFSET);
	if (status & (XAXIDMA_IRQ_IOC_MASK | XAXIDMA_IRQ_DELAY_MASK)) {
		piradio_dma_write32(dma_obj, XAXIDMA_TX_SR_OFFSET, status);
		cur_p = &dma_obj->bd_list[dma_obj->bd_curr];
		status = cur_p->status;
		while (status & XAXIDMA_BD_STS_COMPLETE_MASK) {
			dma_unmap_single(dev->dev.parent, cur_p->phys,
					 cur_p->cntrl &
					 XAXIDMA_BD_CTRL_LENGTH_MASK,
					 DMA_TO_DEVICE);
			skb = (struct sk_buff *)(cur_p->tx_skb);
			pir = (struct piradio_hdr *)skb->data;
			if (cur_p->tx_skb)
				dev_kfree_skb_irq((struct sk_buff *)cur_p->tx_skb);
			cur_p->tx_skb = 0;
			cur_p->status = 0;
			cur_p->cntrl = 0;
			if (++dma_obj->bd_curr >= priv->tx_bd_num)
				dma_obj->bd_curr = 0;
			cur_p = &dma_obj->bd_list[dma_obj->bd_curr];
			status = cur_p->status;
		}
		smp_mb();
		if(netif_queue_stopped(dev)){
			netif_wake_queue(dev);
		}
		return IRQ_HANDLED;
	}
	if (!(status & XAXIDMA_IRQ_ALL_MASK)){
		printk(KERN_ALERT "TX Interrupt received no 2. %d ", status
						);
		return IRQ_NONE;
	}
	if (status & XAXIDMA_IRQ_ERROR_MASK) {
		printk(KERN_ALERT "TX Interrupt received error %d ", status
								);
		return IRQ_NONE;
	}
	return IRQ_HANDLED;
}

int piradio_prep_tx(struct sk_buff *skb, struct net_device *netdev)
{
	unsigned long l_flags;
	int pad;
	int ret;
	int i;
	dma_addr_t tail_base;
	struct piradio_priv *priv = netdev_priv(netdev);
	struct piradio_dma *pdma = &priv->tx_dma;
	struct axidma_bd *cur_p, *next_p;
	u32 nxt_p_offset;
	struct piradio_hdr *pir = skb_push(skb, PIRADIO_HDR_LEN);
	struct tcphdr* tcp_hdr;
	pir->p_type = PIR_T_DATA;
	pir->p_seq_num = htonl(priv->sequence_num++);
	pir->p_csum = ~crc32_le(~0, skb->data + PIRADIO_HDR_LEN, skb->len - PIRADIO_HDR_LEN);
	if (skb->len > 0) {
		if (skb->len % priv->transfer_length != 0) {
			pad = priv->transfer_length - (skb->len % priv->transfer_length);
			ret = skb_pad(skb, pad);
			if (ret < 0) {
				netdev_dbg(priv->netdev,"skb xpad error %d", ret);
				priv->link_stats.tx_dropped++;
				goto error1;
			}
		}
		tcp_hdr = (struct tcphdr*)(skb->data + PIRADIO_HDR_LEN + ETH_HLEN + sizeof(struct iphdr));
		spin_lock_irqsave(&pdma->lock, l_flags);
		cur_p = &pdma->bd_list[pdma->bd_tail];
		uint32_t *mod = skb_push(skb, sizeof(uint32_t)); // Insert modulation;
		*mod = (priv->modulation == 1) ? 1 :
				((priv->modulation == 2) ? 2 :
				((priv->modulation == 4) ? 3 :
				((priv->modulation == 6) ? 4 : 0)));
		cur_p->phys = dma_map_single(priv->dev, skb->data,
				priv->transfer_length + sizeof(uint32_t), DMA_TO_DEVICE);
		cur_p->cntrl = (priv->transfer_length + sizeof(uint32_t)) | XAXIDMA_BD_CTRL_TXSOF_MASK | XAXIDMA_BD_CTRL_TXEOF_MASK;
		cur_p->tx_desc_mapping = 0;
		cur_p->tx_skb = (phys_addr_t)skb;
		tail_base = pdma->bd_list_base + sizeof(*pdma->bd_list) * pdma->bd_tail;
		wmb();
		piradio_dma_bdout(pdma, XAXIDMA_TX_TDESC_OFFSET, tail_base);

		if (++pdma->bd_tail >= priv->tx_bd_num)
				pdma->bd_tail = 0;
		nxt_p_offset = (pdma->bd_tail + 1) % priv->tx_bd_num;
		next_p = &pdma->bd_list[nxt_p_offset];

		if(next_p->cntrl != 0){
			netif_stop_queue(netdev);
			smp_mb();
			spin_unlock_irqrestore(&pdma->lock, l_flags);
			return NETDEV_TX_BUSY;
		}
	}
	spin_unlock_irqrestore(&pdma->lock, l_flags);
	return 0;
error1:
	spin_unlock_irqrestore(&pdma->lock, l_flags);
    do_exit(0);
	return -1;
}

netdev_tx_t piradio1_tx(struct sk_buff *skb, struct net_device *dev)
{
	int ret;
	struct sk_buff *buf_ptr, *tmp, *skb_cp;
	unsigned long l_flags;
	struct piradio_priv* priv = netdev_priv(dev);
	priv->link_stats.tx_packets++;
	piradio_prep_tx(skb, dev);
	return 0;
}

static int piradio1_ioctl(struct net_device *dev, struct ifreq *rq, int cmd)
{
	struct dma_async_tx_descriptor *chan_desc;
	struct piradio_priv* priv = netdev_priv(dev);
	enum dma_ctrl_flags flags = DMA_CTRL_ACK | DMA_PREP_INTERRUPT;
	dma_addr_t mapping;
	unsigned long l_flags;
	int ret;
	int i;
	__u32 *taps;

	struct config_data *conf = (struct config_data *)rq->ifr_ifru.ifru_data;

	switch (cmd) {
	case PIRADIO_CONFIG_MOD:
		if (conf->length) {
			priv->modulation = (conf->length == 1) ? MOD_BPSK :
								(conf->length == 2) ? MOD_QPSK :
								(conf->length == 3) ? MOD_QAM16 : MOD_BPSK;
			if(conf->length >= 1 && conf->length <= 3) {
				write_reg(conf->length, priv->mod_rx_base);
				netdev_dbg(priv->netdev, "Wrote mod");
			}
			else
				write_reg(MOD_BPSK, priv->mod_rx_base);
		}
		return 0;
	case PIRADIO_CONFIG_CORRELATOR:
		break;
	case PIRADIO_CONFIG_FRAMER:
		break;
	case PIRADIO_CONFIG_FFT_TX:
		break;
	case PIRADIO_CONFIG_FFT_RX:
//		if (conf->length) {
//			spin_lock_irqsave(&piradio_p->lock, l_flags);
//			memcpy(test, conf->data, conf->length);
//			ret = config_dma(NULL, (unsigned char *)test,
//					 conf->length, &mapping, DMA_MEM_TO_DEV,
//					 chan_desc, flags,
//					 piradio_p->fft_rx_dma.channel_p,
//					 &piradio_p->pdev->dev,
//					 &piradio_p->fft_rx_dma.cmp,
//					 &piradio_p->fft_rx_dma.cookie,
//					 &corr_cmplt_callback);
//			ret = config_dma(NULL, (unsigned char *)test,
//					 conf->length, &mapping, DMA_MEM_TO_DEV,
//					 chan_desc, flags,
//					 piradio_p->fft_rx2_dma.channel_p,
//					 &piradio_p->pdev->dev,
//					 &piradio_p->fft_rx2_dma.cmp,
//					 &piradio_p->fft_rx2_dma.cookie,
//					 &corr_cmplt_callback);
//
//			spin_unlock_irqrestore(&piradio_p->lock, l_flags);
//		}
		break;
		if (ret == 0) {
			///printk(KERN_ALERT "FFT RX config OK\n");
			return 0;
		} else {
			return -1;
		}
	case PIRADIO_CONFIG_FIR:
//		taps = (__u32 *)conf->data;
//		if (conf->length) {
//			for (i = 0; i < conf->length / 4; i++) {
//				write_reg(taps[i], piradio_p->fir_filt_base +
//							   i * TAP_SIZE);
//			}
//		}
		return 0;
	case PIRADIO_CONFIG_FREQ_OFF:
//		write_reg(conf->length, piradio_p->chann_emm_base);
		return 0;
	case PIRADIO_CONFIG_STHRESH:
//		write_reg(80000, piradio_p->sync_thresh_base);
		return 0;
	case PIRADIO_CONFIG_CSMA:
//		if (conf->length) {
//			write_reg(conf->length, piradio_p->csma_delay_base);
//		}
		break;
	case PIRADIO_CONFIG_ACK:
		printk(KERN_ALERT "Ack enabled %d", conf->length);
		if (conf->length) {
			priv->ack_enabled = 1;
		}
		else{
			priv->ack_enabled = 0;
		}
		break;
	case CONFIG_TRANSFER_LEN:
		priv->transfer_length = conf->length;
		break;
	}
	return 0;
}

static void piradio1_stats(struct net_device *dev, struct rtnl_link_stats64 *stats)
{
	struct piradio_priv* priv = netdev_priv(dev);
	stats->collisions = priv->link_stats.retransmissions;
	stats->tx_packets = priv->link_stats.tx_packets;
	stats->tx_bytes = priv->link_stats.tx_bytes;
	stats->tx_errors = priv->link_stats.tx_errors;
	stats->tx_dropped = priv->link_stats.tx_dropped;
	stats->rx_bytes = priv->link_stats.rx_bytes;
	stats->rx_errors = priv->link_stats.rx_errors;
	stats->rx_packets = priv->link_stats.rx_packets;
	stats->rx_dropped = priv->link_stats.rx_dropped;
	stats->rx_crc_errors = priv->link_stats.crc_rx_errors;
	stats->rx_missed_errors = priv->link_stats.rx_missed_errors;
}

int piradio1_rebuild_header(struct net_device *dev)
{
	return 0;
}

int piradio1_set_mac(struct net_device *dev, void *addr)
{
	return 0;
}

int piradio1_dev_init(struct net_device *dev)
{
	return 0;
}

int piradio1_open(struct net_device *dev)
{
	struct piradio_priv *priv = netdev_priv(dev);
	enum dma_ctrl_flags flags = DMA_CTRL_ACK | DMA_PREP_INTERRUPT;
	int i, ret;

	netif_start_queue(dev);
	memcpy(dev->dev_addr, "PIRAD0", PIRADIO_HW_ALEN);
	printk(KERN_ALERT "PIRADIO0 OPEN");
	priv->link_stats.retransmissions = 0;
	priv->link_stats.rx_bytes = 0;
	priv->link_stats.rx_dropped = 0;
	priv->link_stats.rx_errors = 0;
	priv->link_stats.rx_packets = 0;
	priv->link_stats.tx_bytes = 0;
	priv->link_stats.tx_dropped = 0;
	priv->link_stats.tx_errors = 0;
	priv->link_stats.tx_packets = 0;
	priv->link_stats.rx_missed_errors = 0;
	priv->link_stats.crc_rx_errors = 0;
	priv->tx_tries = 0;
	priv->csma_tries = 0;
	priv->sequence_num = 0;
	priv->ack_enabled = 1;
	priv->modulation = MOD_BPSK;
	priv->transfer_length = OFDM_FRAME_DATA_CARRIERS / (8 / priv->modulation);
	priv->tx_bd_num = 64;
	priv->rx_bd_num = 64;
	priv->coalesce_count_tx = 24;
	priv->coalesce_count_rx = 1;
	napi_enable(&priv->napi);
	ret = request_irq(priv->tx_dma.irq, tx_cmplt_callback,
						  0, dev->name, dev);
	ret = request_irq(priv->rx_dma.irq, rx_cmplt_callback1,
						  0, dev->name, dev);

	piradio_dma_alloc_tx_bds(&priv->tx_dma,
			dev);
	piradio_dma_alloc_rx_bds(&priv->rx_dma,
			dev);

	return 0;
}

static int piradio1_down(struct net_device *dev)
{
	netif_stop_queue(dev);
	return 0;
}

void ret_0(struct net_device *dev)
{
	return;
}

int piradio1_header(struct sk_buff *skb, struct net_device *dev,
		   unsigned short type, const void *daddr, const void *saddr,
		   unsigned int len)
{
	struct piradio_priv *priv = netdev_priv(dev);
	struct ethhdr *eth = skb_push(skb, ETH_HLEN);
	if (type != ETH_P_802_3 && type != ETH_P_802_2)
		eth->h_proto = htons(type);
	else
		eth->h_proto = htons(len);

	if (!saddr)
		saddr = dev->dev_addr;
	memcpy(eth->h_source, saddr, ETH_ALEN);

	if (daddr) {
			memcpy(eth->h_dest, dev->dev_addr,
			       PIRADIO_HW_ALEN);
	}
	return ETH_HLEN;
}

const struct header_ops header_ops ____cacheline_aligned = {
	.create = piradio1_header,
	//	.parse		= ret_0,
	//	.cache		= ret_0,
	//	.cache_update	= ret_0,
	//	.parse_protocol	= ret_0,
};

static const struct net_device_ops piradio1_ops = {
	.ndo_open = piradio1_open,
	.ndo_stop = piradio1_down,
	.ndo_init = piradio1_dev_init,
	.ndo_start_xmit = piradio1_tx,
	.ndo_do_ioctl = piradio1_ioctl,
	.ndo_get_stats64 = piradio1_stats,
};

void piradio1_setup(struct net_device *dev)
{
	ether_setup(dev);
	dev->mtu = 1000;
	dev->max_mtu = 64000;
	dev->hard_header_len = PIRADIO_HDR_LEN + ETH_HLEN; /* 14	*/
	dev->min_header_len = PIRADIO_HDR_LEN + ETH_HLEN; /* 14	*/
	dev->addr_len = PIRADIO_HW_ALEN; /* 6	*/
	dev->flags = IFF_NOARP | IFF_DEBUG | IFF_BROADCAST | IFF_MULTICAST;
	//	dev->priv_flags |= IFF_LIVE_ADDR_CHANGE | IFF_NO_QUEUE ;
	dev->priv_flags |= IFF_TX_SKB_SHARING;
	//	netif_keep_dst(dev);
	//	dev->hw_features = NETIF_F_GSO_SOFTWARE;
	//	dev->features = NETIF_F_SG | NETIF_F_FRAGLIST | NETIF_F_GSO_SOFTWARE |
	//			NETIF_F_NO_CSUM | NETIF_F_RXCSUM | NETIF_F_SCTP_CRC |
	//			NETIF_F_HIGHDMA | NETIF_F_LLTX | NETIF_F_NETNS_LOCAL |
	//			NETIF_F_VLAN_CHALLENGED;
	//	dev->ethtool_ops	= &piradio_ethtool_ops;
	dev->header_ops = &header_ops;
	dev->netdev_ops = &piradio1_ops;
	dev->needs_free_netdev = 1;
	dev->priv_destructor = ret_0;
}

static int piradio1_probe(struct platform_device *pdev)
{
	int rc;
	int err, ret = 1;
	__u8 yes = 0;
	__u8 yess = 0;
	struct device_node* np = NULL;
	struct resource dmares;
	void __iomem *dma_regs;
	struct net_device *netdev = alloc_netdev(sizeof(struct piradio_priv), "piradio1%d", NET_NAME_UNKNOWN, piradio1_setup);
	err = register_netdev(netdev);
	platform_set_drvdata(pdev, netdev);
	SET_NETDEV_DEV(netdev, &pdev->dev);
	struct piradio_priv *priv = netdev_priv(netdev);
	piradio_dma_tx_probe(pdev,
			&priv->tx_dma, 0);
	piradio_dma_rx_probe(pdev,
			&priv->rx_dma, 1);
	piradio_mod_reg_probe(pdev, &priv->mod_rx_base);
	priv->dev = &pdev->dev;
	priv->pdev = pdev;
	netif_napi_add(netdev, &priv->napi, piradio_rx_poll,
			PIRADIO_NAPI_WEIGHT);
	printk(KERN_INFO "dma_proxy module 0 init ok\n");
	return 0;
}

static int piradio1_remove(struct platform_device *pdev)
{
	return 0;
}

static const struct of_device_id piradio1_of_ids[] = {
	{
		.compatible = "xlnx,piradio1",
	},
	{}
};

static struct platform_driver piradio1_driver =
  { .driver =
    { .name = "piradio1_driver", .owner = THIS_MODULE, .of_match_table =
	piradio1_of_ids, }, .probe = piradio1_probe, .remove = piradio1_remove, };

static int __init piradio1_init(void)
{
	printk(KERN_ALERT "Hello World1\n");
	return platform_driver_register(&piradio1_driver);
}

static void piradio1_exit(void)
{
}

module_init(piradio1_init);
module_exit(piradio1_exit);
