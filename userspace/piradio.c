#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "piradio.h"


#define SIZE                     10000
#define SYNC_UNMOD_BYTES         100
#define FFT_SIZE                 1024
#define NUM_TAPS                 10
#define TAPS_SIZE                (NUM_TAPS * 4)
#define COMPLEX_SAMP_SIZE_BYTES  4
#define SYNC_MOD_BYTES           (FFT_SIZE * COMPLEX_SAMP_SIZE_BYTES)
#define RX_PORT                  16888
#define MAXBYTES                 1472
#define ZEROS_SIZE               4
#define FFT_CP_VALUE             0x100
#define NOISE_EST_DATA_LEN       (9 * 80)

#define DATA_OUT_LEN             (1024 * 4 * 9)
#define SWITCH_CTRL_OFFSET       0x00000000
#define SWITCH_MUX_OFFSET        0x00000040

uint8_t buf[SIZE];
uint8_t sync_unmod[SYNC_UNMOD_BYTES];
uint8_t sync_mod[SYNC_MOD_BYTES];
uint32_t taps[NUM_TAPS];
uint8_t buff_fft_tx[ZEROS_SIZE];
uint8_t buff_fft_rx[ZEROS_SIZE];

void
write_to_base(void* base, uint32_t value){
	*((volatile uint32_t*)base) = value;
}

uint32_t
read_from_base(void* base){
	return *((volatile uint32_t*) base);
}

void
map_device(void** mapped_base, int mem_desc, uint32_t addr, uint32_t map_size,
		uint32_t map_mask, uint32_t size){
	void* tmp = mmap(0, map_size, PROT_READ | PROT_WRITE, MAP_SHARED,
			mem_desc, addr & ~map_mask);
	if (tmp == (void*) -1) {
		printf("Can't map the memory to user space.\n");
		exit(0);
	}

	*mapped_base = tmp + (addr & map_mask);

	memset(*mapped_base, 0, size);

	return;
}

void reset_rx_dma_engine(void* base){
	uint32_t ResetMask = (uint32_t) XAXIDMA_CR_RESET_MASK;
	uint32_t ResetMask_rx;
	uint8_t Timeout = 10;

	write_to_base(base + XAXIDMA_RX_OFFSET +
			XAXIDMA_CR_OFFSET, ResetMask);
	do {

		ResetMask_rx = read_from_base(base + XAXIDMA_RX_OFFSET
				+ XAXIDMA_CR_OFFSET);

		if (!(ResetMask_rx & XAXIDMA_CR_RESET_MASK)) {
			break;
		}
		Timeout -= 1;
	} while (Timeout);

	if(Timeout == 0)
		printf("Engine not reset\n");

	return;
}

void config_rx_dma(void* base, uint32_t dst_addr, uint32_t length){
	uint32_t Regvalue = read_from_base(base + XAXIDMA_RX_OFFSET +
			XAXIDMA_CR_OFFSET);
	Regvalue = (uint32_t)(Regvalue | XAXIDMA_CR_RUNSTOP_MASK);
	write_to_base(base + XAXIDMA_RX_OFFSET +
	XAXIDMA_CR_OFFSET,Regvalue); // Start engine

	write_to_base(base + XAXIDMA_RX_OFFSET +
	XAXIDMA_DESTADDR_OFFSET, dst_addr); // Destination address
	write_to_base(base + XAXIDMA_RX_OFFSET +
		XAXIDMA_BUFFLEN_OFFSET, length);
	return;
}

int main(int argc, char* argv[]) {
	int sockfd, sock_rx_corr_fd, sock_tx_fd2, sock_tx_fd3, sock_tx_fd4,
			sock_tx_fd5;
	int size = 2000;
	int memfd;
	struct sockaddr_in servaddr, cliaddr;

	struct sockaddr_in servaddr_tx, cliaddr_tx;
	int n;
	uint32_t fft_tx_val = FFT_CP_VALUE;
	struct ifreq ifr;
	struct config_data conf;
	void *dma_log_base = NULL;
	void *dma_log2_base = NULL;
	void* ddr_rx_base = NULL;
	void* switch_base = NULL;
	void* switch1_base = NULL;
	void* switch2_base = NULL;

	ifr.ifr_ifru.ifru_data = &conf;

	socklen_t len;
	len = sizeof(cliaddr);
	int byte_count = 0;
	int conf_true = 0;
	int tx_data = 0;
	uint32_t nsec = 0;
	int opt;
	int ack = 1;
	int reset=0;
	memfd = open("/dev/mem", O_RDWR | O_SYNC);
	if (memfd == -1) {
		printf("Can't open /dev/mem.\n");
		exit(0);
	}


	while ((opt = getopt(argc, argv, "crfl:n:a:")) != -1) {
		switch (opt) {
		case 'c':
			conf_true = 1;
			break;
		case 'l':
			tx_data = atoi(optarg);
			break;
		case 'n':
			nsec = atoi(optarg);
			break;
		case 'r':
			reset = 1;
			break;
		case 'a':
			ack = atoi(optarg) > 0 ? 1 : 0;
			break;
		default:
			exit(0);
		}
	}
	if(reset){
		memfd = open("/dev/mem", O_RDWR | O_SYNC);
		if (memfd == -1) {
			printf("Can't open /dev/mem.\n");
			exit(0);
		}

		sock_tx_fd2 = socket(AF_INET, SOCK_STREAM, 0);
		if (sock_tx_fd2 == -1) {
			printf("TX socket creation failed...\n");
			exit(0);
		}
		servaddr_tx.sin_family = AF_INET; // IPv4
		servaddr_tx.sin_addr.s_addr = inet_addr("192.168.2.231");
		servaddr_tx.sin_port = htons(16885);
		int ret = connect(sock_tx_fd2, (struct sockaddr*) &servaddr_tx,
				sizeof(servaddr_tx));
		if (ret != 0) {
			printf("connection with the server failed... %d\n", ret);
			//exit(0);
		}

		uint8_t data[DATA_OUT_LEN];
		map_device(&ddr_rx_base, memfd, DDR_BASE_ADDRESS, DDR_MAP_SIZE,
		DDR_MAP_MASK, DATA_OUT_LEN);
		map_device(&dma_log_base, memfd, XPAR_AXIDMA_0_BASEADDR, MAP_SIZE,
		MAP_MASK, 0);

		reset_rx_dma_engine(dma_log_base);
		config_rx_dma(dma_log_base,DDR_BASE_ADDRESS, DATA_OUT_LEN);
	}
	if ((sock_rx_corr_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("socket creation failed");
		exit(EXIT_FAILURE);
	}

	if (conf_true) {
		memfd = open("/dev/mem", O_RDWR | O_SYNC);
		if (memfd == -1) {
			printf("Can't open /dev/mem.\n");
			exit(0);
		}

		memset(buff_fft_rx, 0, ZEROS_SIZE);

		memcpy(buff_fft_tx, &fft_tx_val, ZEROS_SIZE);

		memset(taps, 0, TAPS_SIZE);
		taps[0] = 0x00007fff;
		taps[1] = 0x00000000;
		taps[2] = 0x00000000;
		taps[3] = 0x00000000;
		taps[4] = 0x00000000;
		taps[5] = 0x00000000;
		taps[6] = 0x00000000;
		taps[7] = 0x00000000;
		taps[8] = 0x00000000;
		taps[9] = 0x00000000;

		memset(&servaddr, 0, sizeof(servaddr));
		memset(&cliaddr, 0, sizeof(cliaddr));

		servaddr.sin_family = AF_INET; // IPv4
		servaddr.sin_addr.s_addr = INADDR_ANY;
		servaddr.sin_port = htons(RX_PORT);

		if (bind(sock_rx_corr_fd, (const struct sockaddr *) &servaddr,
				sizeof(servaddr)) < 0) {
			perror("bind failed");
			exit(EXIT_FAILURE);
		}
		while (byte_count < SYNC_UNMOD_BYTES) {
			n = recvfrom(sock_rx_corr_fd, sync_unmod, MAXBYTES, 0,
					(struct sockaddr *) &cliaddr, &len);
			byte_count += n;
		}
		if (n == SYNC_UNMOD_BYTES) {
			conf.data = sync_unmod;
			conf.length = SYNC_UNMOD_BYTES;
			strcpy(ifr.ifr_name, "piradio0");
			ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_FRAMER, &ifr);
			strcpy(ifr.ifr_name, "piradio1");
			ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_FRAMER, &ifr);
		} else {
			printf(
					"Wrong number of sync word bytes for configuring OFDM Framer\n");
			exit(0);
		}

		byte_count = 0;

		while (byte_count < SYNC_MOD_BYTES) {
			n = recvfrom(sock_rx_corr_fd, sync_mod + byte_count, MAXBYTES, 0,
					(struct sockaddr *) &cliaddr, &len);
			printf("recv %d \n", n);
			byte_count += n;
		}

		for (int i = 0; i < NOISE_EST_DATA_LEN; i++) {
			buf[i] = i % 256;
		}

		conf.data = (uint8_t *)taps;
		conf.length = TAPS_SIZE;
		strcpy(ifr.ifr_name, "piradio0");
		ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_FIR, &ifr);
		strcpy(ifr.ifr_name, "piradio1");
		ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_FIR, &ifr);

		conf.data = buff_fft_tx;
		conf.length = ZEROS_SIZE;
		strcpy(ifr.ifr_name, "piradio0");
		ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_FFT_TX, &ifr);
		strcpy(ifr.ifr_name, "piradio1");
		ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_FFT_TX, &ifr);

		conf.data = buff_fft_rx;
		conf.length = ZEROS_SIZE;
		strcpy(ifr.ifr_name, "piradio0");
		ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_FFT_RX, &ifr);
		strcpy(ifr.ifr_name, "piradio1");
		ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_FFT_RX, &ifr);

//		conf.data = NULL;
//		conf.length = 0;
//		ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_PATH, &ifr);

		conf.data = NULL;
		conf.length = 0;
		strcpy(ifr.ifr_name, "piradio0");
		ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_FREQ_OFF, &ifr);
		strcpy(ifr.ifr_name, "piradio1");
		ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_FREQ_OFF, &ifr);

		conf.data = NULL;
		conf.length = 80000;
		strcpy(ifr.ifr_name, "piradio0");
		ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_STHRESH, &ifr);
		strcpy(ifr.ifr_name, "piradio1");
		ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_STHRESH, &ifr);

		conf.data = NULL;
		conf.length = 1023;
		strcpy(ifr.ifr_name, "piradio0");
		ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_CSMA, &ifr);
		strcpy(ifr.ifr_name, "piradio1");
		ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_CSMA, &ifr);

		if (byte_count == SYNC_MOD_BYTES) {
			conf.data = sync_mod;
			conf.length = SYNC_MOD_BYTES;
			strcpy(ifr.ifr_name, "piradio0");
			ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_CORRELATOR, &ifr);
			strcpy(ifr.ifr_name, "piradio1");
			ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_CORRELATOR, &ifr);
		} else {
			printf("Wrong number of bytes for configuring correlator\n");
		}

		printf("Config over\n");

		close(sock_rx_corr_fd);

	}
	conf.data = NULL;
	strcpy(ifr.ifr_name, "piradio0");
	conf.length = ack;
	ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_ACK, &ifr);
	strcpy(ifr.ifr_name, "piradio1");
	ioctl(sock_rx_corr_fd, PIRADIO_CONFIG_ACK, &ifr);
	uint32_t txed;
	size_t total_data;
	int32_t count, tosend;

	if (tx_data > 0) {
			if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
				perror("socket creation failed");
				exit(EXIT_FAILURE);
			}
			cliaddr.sin_family = AF_INET; // IPv4
			cliaddr.sin_addr.s_addr = inet_addr("192.168.3.100");
			cliaddr.sin_port = htons(16682);

	//		if (bind(sockfd, (const struct sockaddr *) &servaddr, sizeof(servaddr))
	//				< 0) {
	//			perror("bind failed");
	//			exit(EXIT_FAILURE);
	//		}

			for (int i = 0; i < SIZE; i++) {
				buf[i] = i % 256;
			}
			int sent = 0;
			while (1) {
				sent = sendto(sockfd, buf, tx_data, 0,
						(const struct sockaddr *) &cliaddr, len);
				if(nsec == 0)
					break;
				else
					usleep(nsec);
			}
		}
}
